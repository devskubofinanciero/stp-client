package mx.com.kubo.stp.client.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mx.com.kubo.messaging.commons.dto.StpDto;
import mx.com.kubo.stp.client.config.StpClients;
import mx.com.kubo.stp.client.exception.TransferRequestException;
import mx.com.kubo.stp.client.exception.WithdrawalTimeoutException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import org.junit.runner.RunWith;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.model.HttpStatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.util.SocketUtils;

import java.io.IOException;
import java.util.HashMap;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {StpClients.class}, initializers = {
  StpClientsTest.TestApplicationContextInitializer.class})
@TestPropertySource(locations = {"classpath:application-test.properties"})
public class StpClientsTest {

  protected static int availablePort;
  private static MockServerClient mockServer;
  @Autowired
  StpClientService stpClientService;

  StpDto dto;
  HashMap<String, String> dtoTransfer;
  HashMap<String, String> dtoTransferOrder;
  ObjectMapper mapper = new ObjectMapper();

  @BeforeClass
  public static void setUp() {
    availablePort = SocketUtils.findAvailableTcpPort();
    mockServer = startClientAndServer(availablePort);
  }

  @AfterClass
  public static void after() {
    mockServer.stop();
  }

  @Before
  public void setup() {

    dto = new StpDto();
    dto.setEnterpriseName("ENTERPRISE");
    dto.setElectronicCertificate("CERTIFICATE");
    dto.setOperatingInstitution("INSTITUTION");
    dto.setOperationDate("DATE");
    dto.setTrackingNumber("NUMBER");


    dtoTransfer = new HashMap<>();
    dtoTransfer.put("nombre", "Rosa Vernica");
    dtoTransfer.put("apellidoPaterno", "Alonso");
    dtoTransfer.put("apellidoMaterno", "Rodrguez");
    dtoTransfer.put("cuenta", "5256783297580590");
    dtoTransfer.put("empresa", "KUBO");
    dtoTransfer.put("rfcCurp", "ubo2022");
    dtoTransfer.put("fechaNacimiento", "ubo2022");
    dtoTransfer.put("paisNacimiento", "mx");
    dtoTransfer.put("firma", "mx");


    dtoTransferOrder = new HashMap<>();
    dtoTransferOrder.put("institucionContraparte", "40002");
    dtoTransferOrder.put("fechaOperacion", "20220217");
    dtoTransferOrder.put("folioOrigen", "kubo20220217906461898153N2022");
    dtoTransferOrder.put("claveRastreo", "kubo20220217906461898153N2022");
    dtoTransferOrder.put("monto", "134.20");
    dtoTransferOrder.put("cuentaOrdenante", "646180142710772792");
    dtoTransferOrder.put("tipoCuentaBeneficiario", "3");
    dtoTransferOrder.put("nombreBeneficiario", "Rosa Vernica Alonso Rodrguez");
    dtoTransferOrder.put("cuentaBeneficiario", "5256783297580590");

    dtoTransferOrder.put("conceptoPago", "Nomina KUBO");
    dtoTransferOrder.put("referenciaNumerica", "2022");
    dtoTransferOrder.put("firma", "EnEPO5NmTMoGXkWs8Id0Twn5qW+JTsaS0qkmT2xKVou58kNONMjkT7tTHNhRmALdNPRYka3M0Da/gWN1paE8uSgegXC4Zsuv1Li3uVUHcMo+Ij/zsWWs/qJ/4TXtLGfqdDZfSzaI5YvAwHxly68nebRU0Y2q4/WNUPm/xUZqDRPzs7YFrzeA9yYyaX02EoVOL4fJMNr+bZZjTVHzxkSZvIXdFL9hZl4GwLMUFHD6X+xvH98r02Wd4vb50R2lCCLlRvu3P8OTi+zdi5+AGr6/a4babrpSfTnGE0ELa+1wR10vgZQylkRwk+cCscah2rRA+5VGfjsnT7R/cO9EaM/i/w==");


    dtoTransferOrder.put("empresa", "KUBO_FINANCIERO");
    dtoTransferOrder.put("institucionOperante", "90646");
    dtoTransferOrder.put("tipoPago", "1");
    dtoTransferOrder.put("tipoCuentaOrdenante", "40");
    dtoTransferOrder.put("nombreOrdenante", "KUBO FINANCIERO");
    dtoTransferOrder.put("tipoOperacion", "1");
    dtoTransferOrder.put("medioEntrega", "3");


  }


  @Test
  public void shouldCalAddTransferAccount() throws JsonProcessingException {
    generateMockAddTransferAccount();
    stpClientService.addTransferAccount(mapper.writeValueAsString(dtoTransfer));
  }

  @Test
  public void shouldCallOrderRegister() throws JsonProcessingException {
    generateMockOrderRegister();
    stpClientService.sendTransferOrder(mapper.writeValueAsString(dtoTransferOrder));
  }

  @Test
  public void shouldCallOrderQuery() throws IOException {
    generateMockTransferQuery();
    stpClientService.sendTransferQuery(dto);
  }

  @Test(expected = WithdrawalTimeoutException.class)
  public void shouldCallOrderQueryTimeOut() throws IOException {
    generateMockTransferQueryTimeOut();
    stpClientService.sendTransferQuery(dto);
  }


  @Test(expected = WithdrawalTimeoutException.class)
  public void shouldCallOrderRegisterTimeOut() throws JsonProcessingException {
    generateMockOrderRegisterTimeOut();
    stpClientService.sendTransferOrder(mapper.writeValueAsString(dtoTransferOrder));
  }

  @Test(expected = TransferRequestException.class)
  public void shouldCallOrderRegisterConflict() throws JsonProcessingException {
    generateMockOrderRegisterConflict();
    stpClientService.sendTransferOrder(dtoTransferOrder);
  }


  void generateMockOrderRegisterConflict() throws JsonProcessingException {
    new MockServerClient("127.0.0.1", availablePort)
      .when(
        request()
          .withMethod("PUT")
          .withHeader(HttpHeaders.USER_AGENT)
          .withHeader(HttpHeaders.HOST)
          .withPath("/speiws/rest/ordenPago/registra")
          .withBody(mapper.writeValueAsString(dtoTransferOrder)),
        exactly(1))
      .respond(
        response()
          .withStatusCode(HttpStatusCode.CONFLICT_409.code())
          .withBody("{\"resultado\":{\"id\":204959704}}")
          .withHeader("Content-Type", "application/json")
      );
  }

  void generateMockTransferQuery() throws IOException {


    new MockServerClient("127.0.0.1", availablePort)
      .when(
        request()
          .withMethod("POST")
          .withHeader(HttpHeaders.USER_AGENT)
          .withHeader(HttpHeaders.HOST)
          .withPath("/speiws/rest/ordenPago/consOrdEnvRastreo")
          .withBody(mapper.writeValueAsString(dto)),
        exactly(1))
      .respond(
        response()
          .withStatusCode(HttpStatusCode.OK_200.code())
          .withBody("{\"resultado\":{\"id\":204959704}}")
          .withHeader("Content-Type", "application/json")
      );
  }

  void generateMockTransferQueryTimeOut() throws IOException {

    new MockServerClient("127.0.0.1", availablePort)
      .when(
        request()
          .withMethod("POST")
          .withHeader(HttpHeaders.USER_AGENT)
          .withHeader(HttpHeaders.HOST)
          .withPath("/speiws/rest/ordenPago/consOrdEnvRastreo")
          .withBody(mapper.writeValueAsString(dto)),
        exactly(1))
      .respond(
        response()
          .withStatusCode(HttpStatusCode.GATEWAY_TIMEOUT_504.code())
          .withBody("{\"resultado\":{\"id\":204959704}}")
          .withHeader("Content-Type", "application/json")
      );
  }


  void generateMockOrderRegister() throws JsonProcessingException {
    new MockServerClient("127.0.0.1", availablePort)
      .when(
        request()
          .withMethod("PUT")
          .withHeader(HttpHeaders.USER_AGENT)
          .withHeader(HttpHeaders.HOST)
          .withPath("/speiws/rest/ordenPago/registra")
          .withBody(mapper.writeValueAsString(dtoTransferOrder)),
        exactly(1))
      .respond(
        response()
          .withStatusCode(HttpStatusCode.OK_200.code())
          .withBody("{\"resultado\":{\"id\":204959704}}")
          .withHeader("Content-Type", "application/json")
      );
  }

  void generateMockAddTransferAccount() throws JsonProcessingException {
    new MockServerClient("127.0.0.1", availablePort)
      .when(
        request()
          .withMethod("PUT")
          .withHeader(HttpHeaders.USER_AGENT)
          .withHeader(HttpHeaders.HOST)
          .withPath("/speiws/rest/cuentaModule/fisica")
          .withBody(mapper.writeValueAsString(dtoTransfer)),
        exactly(1))
      .respond(
        response()
          .withStatusCode(HttpStatusCode.OK_200.code())
          .withBody("{\"resultado\":{\"id\":204959704}}")
          .withHeader("Content-Type", "application/json")
      );
  }

  void generateMockOrderRegisterTimeOut() throws JsonProcessingException {
    new MockServerClient("127.0.0.1", availablePort)
      .when(
        request()
          .withMethod("PUT")
          .withHeader(HttpHeaders.USER_AGENT)
          .withHeader(HttpHeaders.HOST)
          .withPath("/speiws/rest/ordenPago/registra")
          .withBody(mapper.writeValueAsString(dtoTransferOrder)),
        exactly(1))
      .respond(
        response()
          .withStatusCode(HttpStatusCode.GATEWAY_TIMEOUT_504.code())
          .withBody("{\"resultado\":{\"id\":204959704}}")
          .withHeader("Content-Type", "application/json")
      );
  }


  public static class TestApplicationContextInitializer
    implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
      TestPropertySourceUtils
        .addInlinedPropertiesToEnvironment(applicationContext, "stp.mx.api.host=" + "http://127.0.0.1");
      TestPropertySourceUtils
        .addInlinedPropertiesToEnvironment(applicationContext, "stp.mx.api.port=" + availablePort);
    }
  }
}
