package mx.com.kubo.stp.client.service;


import mx.com.kubo.messaging.commons.dto.StpDto;
import mx.com.kubo.stp.client.exception.TransferRequestException;
import mx.com.kubo.stp.client.exception.WithdrawalTimeoutException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class StpClientService {

  private final String STP_REGISTER_ORDER_PATH = "/ordenPago/registra";
  private final String STP_TRANSFER_QUERY_PATH = "/ordenPago/consOrdEnvRastreo";
  private final String STP_ADD_ACCOUNTS_PATH = "/cuentaModule/fisica";
  final static Logger logger = Logger.getLogger(StpClientService.class);


  @Autowired
  @Qualifier("stpClient")
  private RestTemplate stpClient;


  public ResponseEntity<Map> addTransferAccount(Object body) {
    return doExchange(body, STP_ADD_ACCOUNTS_PATH, HttpMethod.PUT);
  }

  public ResponseEntity<Map> sendTransferQuery(StpDto body) {
    return doExchange(body, STP_TRANSFER_QUERY_PATH, HttpMethod.POST);
  }

  public ResponseEntity<Map> sendTransferOrder(Object body) {
    return doExchange(body, STP_REGISTER_ORDER_PATH, HttpMethod.PUT);
  }

  private ResponseEntity<Map> doExchange(Object body, String path, HttpMethod httpMethod) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    try {
      return this.stpClient.exchange(path, httpMethod, new HttpEntity<>(body, headers), Map.class);
    } catch (HttpServerErrorException ex) {
      logger.info("HttpServerErrorException {} " + ex.getStatusCode());
      if (ex.getStatusCode() == HttpStatus.GATEWAY_TIMEOUT)
        throw new WithdrawalTimeoutException("STP gateway timeout");
      else throw new TransferRequestException("STP service unavailable: " + ex.getMessage());
    } catch (RestClientException e) {
      logger.info("RestClientException {} " + e.getLocalizedMessage());
      throw new TransferRequestException("STP service unavailable " + e.getMessage());
    }
  }




}
