package mx.com.kubo.stp.client.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
@ComponentScan("mx.com.kubo.stp.client.service")
public class StpClients {


  @Value("${stp.mx.api.host}")
  private String host;

  @Value("${stp.mx.api.port}")
  private String port;

  @Value("${stp.mx.api.path}")
  private String path;


  @Bean
  @Qualifier("stpClient")
  public RestTemplate stpClient() {
    return new RestTemplateBuilder()
      .setConnectTimeout(Duration.ofSeconds(5))
      .setReadTimeout(Duration.ofSeconds(15))
      .rootUri(getFullUrl())
      .build();
  }

  protected String getFullUrl() {
    return new StringBuilder()
      .append(this.host)
      .append(':')
      .append(this.port)
      .append(this.path)
      .toString();
  }


}
