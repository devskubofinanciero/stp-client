package mx.com.kubo.stp.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TransferRequestException extends ResponseStatusException {
  public TransferRequestException(String message) {
    super(HttpStatus.CONFLICT, message);
  }
}
