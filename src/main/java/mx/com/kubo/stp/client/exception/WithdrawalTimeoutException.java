package mx.com.kubo.stp.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class WithdrawalTimeoutException extends ResponseStatusException {
  public WithdrawalTimeoutException(String message) {
    super(HttpStatus.CONFLICT, message);
  }
}
